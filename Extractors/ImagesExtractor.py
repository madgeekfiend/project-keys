from bs4 import BeautifulSoup, SoupStrainer
import Singletons.http_lib


class ImagesExtractor:
    """
    Extract images from url

    Follows Interface patter of having:
    self.name and def extract(self, url) for duck typing
    """
    def __init__(self):
        self.http = Singletons.http_lib.global_http
        self.name = "images"

    def extract(self, url):
        """
        Extract all 'img' tags from page and returns source

        :param url: url of page to extract from
        :return: list of images
        """
        status, response = self.http.request(url)
        images = set()

        for image in BeautifulSoup(response, parse_only=SoupStrainer('img')):
            if image.has_attr('src'):
                # it's an image link add it to set
                images.add(image["src"])

        return list(images)
