from bs4 import BeautifulSoup, SoupStrainer
from urllib.parse import urlparse
from tld import get_tld
from tld.utils import update_tld_names
import Singletons.http_lib
import validators

class ExternalDomainLinkExtractor:
    """
    Get all external links from a page

    Follow duck type pattern of self.name and def extract(self, url)
    as I emulate an interface
    """
    def __init__(self):
        self.http = Singletons.http_lib.global_http
        self.name = "external_links"
        update_tld_names()

    def extract(self, url):
        status, response = self.http.request(url)
        top_level_domain = get_tld(url)
        urls = set()

        for link in BeautifulSoup(response, parse_only=SoupStrainer('a')):
            if link.has_attr('href'):
                # it's an image link add it to set
                internal_link = link['href']
                if urlparse(internal_link).netloc != top_level_domain and validators.url(internal_link):
                    urls.add(internal_link)

        return list(urls)
