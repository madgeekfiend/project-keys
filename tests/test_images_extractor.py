import unittest
from Extractors.ImagesExtractor import ImagesExtractor


class TestImagesExtractor(unittest.TestCase):
    def test_create_valid_instance(self):
        obj = ImagesExtractor()
        self.assertIsInstance(obj, ImagesExtractor)


if __name__ == "__main__":
    unittest.main()
