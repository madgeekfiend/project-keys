import unittest
from Extractors.InternalDomainLinkExtractor import InternalDomainLinkExtractor


class TestInternalDomainLinkExtractor(unittest.TestCase):
    def test_create_valid_instance(self):
        obj = InternalDomainLinkExtractor()
        self.assertIsInstance(obj, InternalDomainLinkExtractor)


if __name__ == "__main__":
    unittest.main()
