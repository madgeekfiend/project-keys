import unittest
from Extractors.ExternalDomainLinkExtractor import ExternalDomainLinkExtractor


class TestExternalDomainLinkExtractor(unittest.TestCase):
    def test_create_valid_instance(self):
        obj = ExternalDomainLinkExtractor()
        self.assertIsInstance(obj, ExternalDomainLinkExtractor)


if __name__ == "__main__":
    unittest.main()
