import unittest
from Spider.SingleDomainPageCrawler import SingleDomainPageCrawler
from Extractors.ImagesExtractor import ImagesExtractor


class TestSingleDomainPageCrawler(unittest.TestCase):
    """
    Test the SingleDomainPage Crawler
    """

    def test_create_instance_with_no_extractors(self):
        obj = SingleDomainPageCrawler("http://testing.com")
        self.assertIsInstance(obj, SingleDomainPageCrawler)

    def test_create_instance_with_extractor(self):
        obj = SingleDomainPageCrawler("http://testing.com", extractors=[ImagesExtractor()])
        self.assertIsInstance(obj, SingleDomainPageCrawler)

if __name__ == "__main__":
    unittest.main()
