from Extractors.InternalDomainLinkExtractor import InternalDomainLinkExtractor
import logging


class SingleDomainPageCrawler:
    """
    Onde Domain Crawler

    This only crawls the one domain and links to that domain
    """

    def __init__(self, starting_url, extractors=None):
        if extractors is None:
            extractors = [InternalDomainLinkExtractor()]
        logging.info("Initializing one domain crawler to crawl domain: {0}".format(starting_url))
        self.starting_url = starting_url
        self.external_links = []
        self.internal_links = []
        self.extractors = extractors
        self.crawl_info = {}

    def crawl(self):
        visit_pages_queue = [self.starting_url]
        pages_already_visited = set()
        number_pages_visited = 0

        # just look at queue size
        while visit_pages_queue:
            deduped_links = set()
            logging.info("QUEUE SIZE: {0}".format(len(visit_pages_queue)))
            number_pages_visited += 1
            url_to_visit = visit_pages_queue[0]
            # remove the page from the queue by dropping the first array item
            # but add to set for pages_already_visited
            pages_already_visited.add(url_to_visit)
            # extraction
            logging.info("Visiting URL: {0}".format(url_to_visit))
            # Run extractors
            for extractor in self.extractors:
                # Use an extractor and get internal links
                if url_to_visit in self.crawl_info:
                    self.crawl_info[url_to_visit].update({extractor.name: extractor.extract(url_to_visit)})
                else:
                    self.crawl_info[url_to_visit] = {extractor.name: extractor.extract(url_to_visit)}
            internal_links = self.crawl_info[url_to_visit]["internal_links"]  # page_extractor.extract(url_to_visit)
            logging.info("Added {0} URLs to page queue".format(len(internal_links)))
            for link in internal_links:
                if link not in pages_already_visited:
                    deduped_links.add(link)
            visit_pages_queue = visit_pages_queue[1:]
            visit_pages_queue.extend(deduped_links)
            # dedupe queue
            # For some reason creating a new set and then casting to list feels 'fishy'
            # NOTE TO SELF explore python garbage collection
            visit_pages_queue = list(set(visit_pages_queue))
