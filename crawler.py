import sys
import json

from Spider.SingleDomainPageCrawler import SingleDomainPageCrawler
from Extractors.InternalDomainLinkExtractor import InternalDomainLinkExtractor
from Extractors.ExternalDomainLinkExtractor import ExternalDomainLinkExtractor
from Extractors.ImagesExtractor import ImagesExtractor
import logging


def main():
    """
    Main program execution

    The crawler uses the strategy pattern to determine what to extract. You include in an array
    the instantiated object you want to extract. This helps to isolate our extraction code. Given more
    time I would have done the same for the crawler

    :return:
    """
    logging.basicConfig(filename='crawler.log', level=logging.INFO)
    # Add stream handler to log to console
    stdout_logger_handler = logging.StreamHandler(sys.stdout)
    stdout_logger_handler.setLevel(logging.INFO)
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    stdout_logger_handler.setFormatter(formatter)
    logging.getLogger().addHandler(stdout_logger_handler)
    starting_domain = "http://wiprodigital.com/"
    crawler = SingleDomainPageCrawler(starting_domain, extractors=[InternalDomainLinkExtractor(),
                                                                   ExternalDomainLinkExtractor(),
                                                                   ImagesExtractor()])
    crawler.crawl()
    logging.info("LINKS: {0}".format(len(crawler.internal_links)))
    output = {"pages": crawler.crawl_info}
    # Output crawler info to file pretty human readable version and
    # as JSON
    with open('crawl_info.json', 'wt') as json_out:
        json.dump(output, json_out)

    with open('crawl_info-pretty.json', 'wt') as out:
        json.dump(output, out, sort_keys=True, indent=4)


if __name__ == "__main__":
    main()
