# Fun Assignment

## Requirements

* Python 3.6.1
* Internet Connection

## Development Environment

IntelliJ 2017 Ultimate with Python plugin and Mac OSX Sierra

## How to install

If you have python 3.6.1 installed. Goto the command line
 on a unix or Mac based system and run `pip install -r requirements.txt`. This
 will install all the required python libraries.
 
## Install with pyenv & virtualenv

Pyenv allows for easy separation of your python exectuables and
environments so you don't run into library incompatibilities. You can
install pyenv from [pyenv-installer](https://github.com/pyenv/pyenv-installer)

Once you installed it on Mac OS X or *nix type system. You can run these
commands:

```$xslt
pyenv install 3.6.1
pyenv global 3.6.1 
pyenv virtualenv webcrawler
pyenv activate webcrawler
```

This will set your global python executable to use version 3.6.1. Create
a seperate virtual env that will store all required python libraries 
without conflicting with your existing libraries and run this
code in isolation.

I have included a _.pyenv-version_ file that sets this to use version
3.6.1 of Python

## Testing

Tests are located in the `/tests` directory. Before shipping to production this requires more thorough testing
with mocks but due to time constraints was unable to put that in. You can run tests from the CLI:

`python -m unittest`

As test discovery compatability has been met due to naming conventions.

## Running

Change into the directory you cloned the repository into and run

`python crawler.py`

### Output

You will get 2 text files one is JSON and the other is JSON _prettified_ the names 
will be *crawler_info-pretty.json* and *crawler_info.json*

# Overview of code

The domain name is hard wired into the code. Given more time I would push this to either command line or a
persistance layer. I built a crawler that only crawls links internal to the domain. What happens is that
it grabs the links and goes to those links and extracts images, internal links, and external links for the given
url.

# What would I do with more time

**WRITE TESTS!**

## Use Scrapy

[Scrapy](https://scrapy.org/) is built for this and follows the **Django** style for 
development and layout. If I had more time like a day or so I would have read up on this
and determine if it would have worked for our use-case.

## If I had to keep current architecture

If I was going to keep the current architecture of a monolithic application. I would
break out the crawler logic so the user would be able to specify whatever URL they wanted. I would
also try and use the **strategy** pattern with the crawler.

I would also put the crawler and extractors in it's on thread using a **thread connection pool** 
in order to preserve resources. I didn't have time to debug any shared memory or race conditions.

## Pie in the sky **MICROSERVICES**

I would implement an _Enterprise Service Bus_ using something like (ActiveMQ, RabbitMQ or ZeroMQ). If I 
chose ActiveMQ I would setup a cluster to create _jobs_. This would have it's 
own persistance layer and track what urls needed refreshing. 

When it deterimines what URLs those are using the _ActiveMQ_ it would send a job
message to another **cluster** I would have setup for crawling. I would also add in a data repository or
warehouse that stored the HTML of the url that was retrieved. Crawlers would be updated to 
look at cache if nothing has changed don't retrieve.

With this information another cluster let's call it **Extractors** would then process the
HTML for the files.

This gives us the benefit of being able to scale **Horizontally** by adding more servers 
to the cluster when load gets high. This would be easily done with the autoscaling services offered
 by Amazon AWS, Heroku, EngineYard, Google App Engine, Azure, etc...

